# ansible_debian

Ansible plays to be executed on debian hosts

## Task list

- **packages.yml** Installs basic packages and updates all installed packages
- **users.yml** Creates an ansible system user and adds sudo rights
- **cron.yml** Add automatic provisioning via ansible pull cron jobs
